import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Main from './pages/Main';

import {store} from './store';

const routes = [
	{
		name: 'list',
		path: '/',
		component: Main,
		beforeEnter(to, from, next) {
			let debugValue = to.query['debug'];
			if(debugValue) {
				store.dispatch('global/changeDebug', debugValue.toLowerCase() == 'true');
			}
			next();
		}
	}
];

export default new VueRouter({
	routes,
	mode: 'history'
})