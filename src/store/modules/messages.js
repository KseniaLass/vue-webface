import Vue from 'vue';
import axios from 'axios';

export default {
    namespaced: true,
    state: {
        messagesDefault: {
            messages: {},
            cols: [0, 0, 0, 0]
        },
        messagesIgnore: {
            messages: {},
            cols: [0, 0, 0, 0]
        },
        ignoreMessagesList: {},
        loadDone: false,
        socket: {
            // state: 'connect',
            // message: 'CONNECTING...'
            state: 'preloader'
        },
        countMoney: 0,
        countSuperMoney: 0,
        countMoneyPercent: 0,
        socketObject: null,
        dwCache: {},
        balancerCache: {},
        libraryInfo: {}
    },
    getters: {
        socketState(state) {
            return state.socket;
        },
        loadDone(state) {
            return state.loadDone;
        },
        getMessages(state){
            return state.messagesDefault.messages
        },
        getIgnoreMessages(state) {
            return state.messagesIgnore.messages
        },
        getCol: (state) => (col, ignore) => {
            let result = [];
            let collections = ignore ? state.messagesIgnore.messages : state.messagesDefault.messages;

            if(Object.keys(collections).length) {
                for(let i in collections) {
                    if(collections[i].col === col) {
                        result.push(collections[i]);
                    }
                }
                return result
            }
        },
        getCountMoney(state) {
            return state.countMoney;
        },
        getCountSuperMoney(state) {
            return state.countSuperMoney;
        },
        getCountMoneyPercent(state) {
            return state.countMoneyPercent;
        },
        getDWCacheDeposit: (state) => (exchange, coin) => {
            return state.dwCache[`${exchange}/${coin}`].response.deposit;
        },
        getDWCacheWithdrawal: (state) => (exchange, coin) => {
            return state.dwCache[`${exchange}/${coin}`].response.withdrawal;
        },
        getGlobalDWChekerURL(state, getters, rootState, rootGetters) {
            return rootGetters['global/getDWCheckerURL'];
        },
        getGlobalBalancerURL(state, getters, rootState, rootGetters) {
            return rootGetters['global/getBalancerURL'];
        },
        getGlobalSocketURL(state, getters, rootState, rootGetters) {
            return rootGetters['global/getSocketURL'];
        },
        getLibraryURL(state, getters, rootState, rootGetters) {
            return rootGetters['global/getLibraryURL'];
        },
        getIgnoreMessagesList(state) {
            return state.ignoreMessagesList;
        },
        getBalancerCache: (state) => (exchange, coin) => {
            return state.balancerCache[`${exchange}/${coin}`].response
        },
        getDataFromLibraryInfo: (state) => (coin) => {
            for(let info in state.libraryInfo) {
                if(info === coin) {
                    return state.libraryInfo[info]
                }
            }
        }
    },
    mutations: {
        SOCKET_ONOPEN: (state, event) => {
            state.socket.state = 'open';
            state.socket.message = `CONNECTED to ${event.currentTarget.url}. WAITING FOR DATA.`;
        },
        addMessage(state, data) {
            /**
             * @param object state state of Vuex
             * @param object data {message, collection: 'messagesDefault' || 'messagesIgnore'}
             * 
             */

            state.socket.state = 'open';
            state.loadDone = true;

            let message = data.message;
            let collection = state[data.collection].messages;
            let cols = state[data.collection].cols;

            //Check isset in storage ignore
            // let collection = ignore ? state.ignoreMessages : state.messages;
            // let cols = ignore ? state.ignoreCols : state.cols;

            if(!collection.hasOwnProperty(message.coin)) {
                // If coin not isset, create new coin in state.messages
                Vue.set(collection, message.coin, {});
                let min = getMinCol(cols) || 0;

                cols[min] = cols[min] + 1;
                Vue.set(collection[message.coin], 'col', min);
                Vue.set(collection[message.coin], 'coin', message.coin);
                //Vue.set(collection[message.coin], 'anticalc', message.anticalc);
                Vue.set(collection[message.coin], 'explorer', message.explorer);
                Vue.set(collection[message.coin], 'data', {});
            }

            // Update/add message to Coin object by ID
            Vue.set(collection[message.coin].data, message.id, message);

        },
        removeMessage(state, data) {
            /**
             * @param object state state of Vuex
             * @param object data {message, collection: 'messagesDefault' || 'messagesIgnore'}
             * 
             */

            let message = data.message;
            let collection = state[data.collection].messages;
            let cols = state[data.collection].cols;

            if(Object.keys(collection).length > 0) {
                for(let i in collection) {
                    let coin = collection[i];
                    if(coin.data.hasOwnProperty(message.id)) {
                        Vue.delete(coin.data, message.id);
                        if(Object.keys(coin.data).length === 0) {
                            let col = collection[i].col;
                            cols[col] = cols[col] - 1;
                            Vue.delete(collection, i);
                        }
                    }
                }
            }
        },
        SOCKET_ONERROR: (state, event) => {
            state.socket.state = 'error';
            state.socket.message = 'ERROR';
        },
        SOCKET_ONCLOSE: (state, event) => {
            state.loadDone = true;
            state.socket.state = 'close';
            state.socket.message = `DISCONNECTED. Code: ${event.code}.`;
        },
        setCountMoney(state, val) {
            state.countMoney = val
        },
        setCountSuperMoney(state, val) {
            state.countSuperMoney = val
        },
        setCountMoneyPercent(state, val) {
            state.countMoneyPercent = val
        },
        createKeyInCache(state, data) {
            state[data.cache][data.key] = {
                status: 'pending',
                response: false
            };
        },
        setDataToCache(state, data) {
            state[data.cache][data.key] = {
               response: data.response,
               timestamp: data.timestamp,
               status: 'success'
            }
        },
        setSocketObject(state, url) {
            state.socketObject = new WebSocket(url);
        },
        deleteSocketObject(state) {
            state.socketObject = null;
        },
        clearAllData(state) {
            state.messagesDefault = {
                messages: {},
                cols: [0, 0, 0, 0]
            },
            state.messagesIgnor = {
                messages: {},
                cols: [0, 0, 0, 0]
            },
            state.loadDone = false;
            state.socket = {
                // state: 'connect',
                // message: 'CONNECTING...'
                state: 'ready'
            };
            state.cols = [0, 0, 0];
            state.socketObject = null;
        },
        addIgnoreMessagesList(state, data) {
            if(state.ignoreMessagesList[data.coin]) {
                state.ignoreMessagesList[data.coin].push(data.exchange)
                //Vue.set(state.ignoreMessagesList[data.coin], data.exchange)
            } else {
                Vue.set(state.ignoreMessagesList, data.coin, []);
                Vue.set(state.ignoreMessagesList[data.coin], 0, data.exchange)
            }
        },
        removeIgnoreMessagesList(state, data) {
            let index = state.ignoreMessagesList[data.coin].indexOf(data.exchange);
            Vue.delete(state.ignoreMessagesList[data.coin], index);
            if(state.ignoreMessagesList[data.coin].length === 0) {
                Vue.delete(state.ignoreMessagesList, data.coin);
            }
        },
        setLibraryInfo(state, data) {
            state.libraryInfo = data;
        }
    },
    actions: {
        changeCountMoney(store, val) {
            store.commit('setCountMoney', val)
        },
        changeCountSuperMoney(store, val) {
            store.commit('setCountSuperMoney', val)
        },
        changeCountMoneyPercent(store, val) {
            store.commit('setCountMoneyPercent', val);
        },
        socketCreate(store, url) {
            if(store.state.socketObject) {
                store.state.socketObject.close();
                store.commit('deleteSocketObject');
                store.commit('clearAllData');
            }

            store.commit('setSocketObject', url);

            let updateTimeout = null

            store.state.socketObject.onopen = (e) => store.commit('SOCKET_ONOPEN', e);
            store.state.socketObject.onmessage = async (data) => {
                let message = JSON.parse(data.data);
                let currentTime = new Date() / 1000;
                let dwCheckerURL = store.getters.getGlobalDWChekerURL;
                let balancerURL = store.getters.getGlobalBalancerURL;

                let isIgnore = checkIgnoreMessage(message, store.state.ignoreMessagesList);

                if(message.event === 'update') {

                    let coin = message.coin,
                        buyExchange = message.data.buy_exchange,
                        sellExchange = message.data.sell_exchange,
                    
                        buyKey = `${buyExchange}/${coin}`,
                        sellKey = `${sellExchange}/${coin}`;

                    // Get balancer

                    if(store.state.balancerCache[buyKey] === undefined || (currentTime - store.state.balancerCache[buyKey] > 60) && !store.state.balancerCache[buyKey].response) {

                        store.commit('createKeyInCache', {
                            key: buyKey,
                            cache: 'balancerCache'
                        });
                        let balance = await getAJAXInfo(buyExchange, coin, balancerURL);

                        if(balance.data) {
                            store.commit('setDataToCache', {
                                cache: 'balancerCache',
                                key: buyKey,
                                timestamp: currentTime,
                                response: balance.data ? balance.data : {}
                            });
                        } else {
                            console.log(balance)
                        }
                    }
                    
                    // Get DWChecker data
                    //    Buy exchange
                    if(store.state.dwCache[buyKey] === undefined || (currentTime - store.state.dwCache[buyKey].timestamp > 60) && !store.state.dwCache[buyKey].response) {

                        store.commit('createKeyInCache', {
                            key: buyKey,
                            cache: 'dwCache'
                        });
                        let dwBuy = await getAJAXInfo(buyExchange, coin, dwCheckerURL);
                        if(dwBuy.data) {
                            store.commit('setDataToCache',  {
                                cache: 'dwCache',
                                key: buyKey,
                                timestamp: currentTime,
                                response: dwBuy.data ? dwBuy.data : {}
                            });
                        } else {
                            console.log(dwBuy);
                        }
                    }
                    //    Sell exchange
                    if(store.state.dwCache[sellKey] === undefined || (currentTime - store.state.dwCache[sellKey].timestamp > 60) && !store.state.dwCache[buyKey].response) {
                        store.commit('createKeyInCache', {
                            key: sellKey,
                            cache: 'dwCache'
                        });
                        let dwSell = await getAJAXInfo(sellExchange, coin, dwCheckerURL);
                        if(dwSell.data) {
                            store.commit('setDataToCache', {
                                cache: 'dwCache',
                                key: sellKey,
                                timestamp: currentTime,
                                response: dwSell.data ? dwSell.data : {}
                            });
                        } else {
                            console.log(dwSell)
                        }
                    }
                    store.commit('addMessage', {message, collection: isIgnore ? 'messagesIgnore' : 'messagesDefault'});

                } else if (message.event === 'remove') {
                    store.commit('removeMessage', {message, collection: isIgnore ? 'messagesIgnore' : 'messagesDefault'});

                }

                if(process.env.NODE_ENV === 'production') {
            
                    // Очищаем таймер после обработки нового сообщения
                    clearTimeout(updateTimeout);

                    // Запускаем таймер для отслеживания последнего обновления, если таймер не очистился новым сообщением спустя 2 минуты, 
                    // значит сокет не работает и его нужно перезапустить
                    updateTimeout = setTimeout(function(){
                        console.log('bs timer');
                        store.state.socketObject.close();
                        store.commit('deleteSocketObject');
                        store.commit('clearAllData');
                        store.dispatch('socketCreate', store.getters.getGlobalSocketURL)
                    }, 120000);

                }

            };
            //store.state.socketObject.onerror = (e) => store.commit('SOCKET_ONMESSAGE', e);
            store.state.socketObject.onclose = (e) => store.commit('SOCKET_ONCLOSE', e);
                
            if(process.env.NODE_ENV === 'development') {
                setTimeout(function(){
                    console.log('stop socket')
                    //store.state.socketObject.onclose = function () {}; // disable onclose handler first
                    store.state.socketObject.onclose = (e) => {};
                    store.state.socketObject.close();
                }, 5000)
            }
        },
        socketRemove(store) {
            store.state.socketObject.close();
            store.commit('deleteSocketObject');
            //store.commit('SOCKET_ONCLOSE', e);
        },
        setIgnoreMessageList(store, data) {
            for (var i = 0; i < localStorage.length; i++) {
                let key = localStorage.key(i);
                if(key.indexOf('IGNORE:') > -1) {
                    let coin = key.substr(8).split('/')[0],
                        exchange = key.substr(8).split('/')[1];
                    store.commit('addIgnoreMessagesList', {coin, exchange});
                }
            }
        },
        moveMessage(store, data) {
            let coin = data.coin,
                exchange = data.exchange.toLowerCase(),
                action = data.action,
                messagesArrayForCoin = store.state.messagesIgnore.messages[coin],
                oldCollection = 'messagesIgnore',
                newCollection = 'messagesDefault';

                if(action === 'ignore') {
                    messagesArrayForCoin = store.state.messagesDefault.messages[coin];
                    oldCollection = 'messagesDefault';
                    newCollection = 'messagesIgnore';

                    localStorage.setItem(`IGNORE: ${data.coin}/${data.exchange}`, true);
                    store.commit('addIgnoreMessagesList', data);
                } else {
                    localStorage.removeItem(`IGNORE: ${data.coin}/${data.exchange}`)
                    store.commit('removeIgnoreMessagesList', data);
                }

            if(messagesArrayForCoin) {
                for(let i in messagesArrayForCoin.data) {
                    if(messagesArrayForCoin.data[i].data.buy_exchange.toLowerCase() === exchange || messagesArrayForCoin.data[i].data.sell_exchange.toLowerCase() === exchange) {
                        store.commit('addMessage', {message: messagesArrayForCoin.data[i], collection: newCollection})
                        store.commit('removeMessage', {message: messagesArrayForCoin.data[i], collection: oldCollection})
                    }
                }
            }
        },
        async requestLibrary(store) {
            let lib = await getLibraryInfo(store.getters.getLibraryURL);
            if(lib.statusText === 'OK') {
                store.commit('setLibraryInfo', lib.data)
            } else {
                console.log(lib);
            }
            
        }
    }
}
function getMinCol(array) {
    return array.indexOf(Math.min(...array))
}
function checkLocalStorage(val) {
    return localStorage.getItem(val);
}
async function getAJAXInfo(exchange, coin, url) {
    try {
        const response = await axios.get(url, {
            params: {
              exchange,
              coin
            }
        });
        return response
    } catch (error) {
        return error
    }
}
async function getLibraryInfo(url) {
    console.log(url)
    try {
        const response = await axios.get(url);
        return response
    } catch (error) {
        return error
    }
}
function checkIgnoreMessage(message, ignoreCollection) {

    let coin = message.event === 'update' ? message.coin : message.data.coin,
        sell_exchange = message.data.sell_exchange.toLowerCase(),
        buy_exchange = message.data.buy_exchange.toLowerCase();

    if(Object.keys(ignoreCollection).length > 0) {
        if(ignoreCollection[coin]) {
            return ignoreCollection[coin].some(item=>{
                if(item.toLowerCase() === buy_exchange || item.toLowerCase() === sell_exchange) {
                    return true
                }
            })
        } else {
            return false
        }
    } else {
        return false
    }

}