export default {
    namespaced: true,
    state: {
        isDebug: false,
        background: '',
        socketURL: 'ws://nerfbear.com:15678/',
        dwCheckerURL: 'http://quoshkqua.com:55555/assetInfo',
        balancerURL: 'http://quoshkqua.com:44441/labustoken',
        libraryURL: 'http://35.228.25.41:5001/info'
    },
    getters: {
        getDebug(state) {
            return state.isDebug;
        },
        getBackground(state) {
            return state.background
        },
        getSocketURL(state) {
            return state.socketURL
        },
        getDWCheckerURL(state) {
            return state.dwCheckerURL
        },
        getBalancerURL(state) {
            return state.balancerURL
        },
        getLibraryURL(state) {
            return state.libraryURL
        }
    },
    mutations: {
        setDebug(state, payload) {
            state.isDebug = payload;
        },
        setBackground(state, val) {
            state.background = val;
            document.body.style.background = val
        },
        setSocketURL(state, val) {
            state.socketURL = val;
        },
        setDWCheckerURL(state, val) {
            state.dwCheckerURL = val;
        },
        setBalancerURL(state, val) {
            state.balancerURL = val;
        },
        setLibraryURL(state, val) {
            state.libraryURL = val;
        }
    },
    actions: {
        changeDebug(store, payload) {
            store.commit('setDebug', payload);
        },
        changeBackground(store, val) {
            store.commit('setBackground', val);
        },
        changeSocketURL(store, val) {
            store.commit('setSocketURL', val);
        },
        changeDWCheckerURL(store, val) {
            store.commit('setDWCheckerURL', val);
        },
        changeBalancerURL(store, val) {
            store.commit('setBalancerURL', val);
        },
        changeLibraryURL(store, val) {
            store.commit('setLibraryURL', val);
        }
    }
};