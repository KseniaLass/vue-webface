import Vue from 'vue';
export default {
    namespaced: true,
    state: {
        updateCollection: [],
        loadDone: false,
        activeGroup: '',
        socket: {
            state: 'connect',
            message: 'CONNECTING...'
        }
    },
    getters: {
        getUpdater(state) {
            return state.updateCollection
        },
        getActiveUpdater(state) {
            let res = state.updateCollection.filter(group => {
                return group.name === state.activeGroup
            })[0] || state.updateCollection[0];
            return res
        },
        socketState(state) {
            return state.socket;
        },
        loadDone(state) {
            return state.loadDone;
        }
    },
    mutations: {
        SOCKET_ONOPEN: (state, event) => {
            state.socket.state = 'open';
            state.socket.message = 'CONNECTED. WAITING FOR DATA...';
        },
        SOCKET_ONMESSAGE: (state, message) => {
            state.socket.state = 'ready';
            state.loadDone = true;
            state.updateCollection = message;
            if(localStorage.getItem('active-group')) {
                state.activeGroup = localStorage.getItem('active-group')
            } else {
                state.activeGroup = message[0].name;
            }
        },
        SOCKET_ONERROR: (state, event) => {
            state.socket.state = 'error';
            state.socket.message = 'ERROR';
        },
        SOCKET_ONCLOSE: (state, event) => {
            state.loadDone = true;
            state.socket.state = 'close';
            state.socket.message = `DISCONNECTED. Code: ${event.code}`;
        },
        setActiveGroup(state, name) {
            state.activeGroup = name;
        }
    },
    actions: {
        socketOpen(store, event) {
            store.commit('SOCKET_ONOPEN', event);
        },
        socketMessage(store, data) {
            store.commit('SOCKET_ONMESSAGE',JSON.parse(data.data))
        },
        socketError(store, event) {
            store.commit('SOCKET_ONERROR', event);
        },
        socketClose(store, event) {
            store.commit('SOCKET_ONCLOSE', event);
        },
        changeActiveGroup(store, name) {
            store.commit('setActiveGroup', name);
        }
    }
}